from scipy.io import wavfile as wf
import wave
import numpy as np
import pandas as pd
import os
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from ipywidgets import IntProgress
from IPython.display import display

def read_wav_file(str_filename, target_rate):
    wav = wave.open(str_filename, mode = 'r')
    (sample_rate, data) = extract2FloatArr(wav,str_filename)
    
    if (sample_rate != target_rate):
        ( _ , data) = resample(sample_rate, data, target_rate)
        
    wav.close()
    return (target_rate, data.astype(np.float32))

def resample(current_rate, data, target_rate):
    x_original = np.linspace(0,100,len(data))
    x_resampled = np.linspace(0,100, int(len(data) * (target_rate / current_rate)))
    resampled = np.interp(x_resampled, x_original, data)
    return (target_rate, resampled.astype(np.float32))

# -> (sample_rate, data)
def extract2FloatArr(lp_wave, str_filename):
    (bps, channels) = bitrate_channels(lp_wave)
    
    if bps in [1,2,4]:
        (rate, data) = wf.read(str_filename)
        divisor_dict = {1:255, 2:32768}
        if bps in [1,2]:
            divisor = divisor_dict[bps]
            data = np.divide(data, float(divisor)) #clamp to [0.0,1.0]        
        return (rate, data)
    
    elif bps == 3: 
        #24bpp wave
        return read24bitwave(lp_wave)
    
    else:
        raise Exception('Unrecognized wave format: {} bytes per sample'.format(bps))
        
def read24bitwave(lp_wave):
    nFrames = lp_wave.getnframes()
    buf = lp_wave.readframes(nFrames)
    reshaped = np.frombuffer(buf, np.int8).reshape(nFrames,-1)
    short_output = np.empty((nFrames, 2), dtype = np.int8)
    short_output[:,:] = reshaped[:, -2:]
    short_output = short_output.view(np.int16)
    return (lp_wave.getframerate(), np.divide(short_output, 32768).reshape(-1))  #return numpy array to save memory via array slicing

def bitrate_channels(lp_wave):
    bps = (lp_wave.getsampwidth() / lp_wave.getnchannels()) #bytes per sample
    return (bps, lp_wave.getnchannels())

def slice_data(start, end, raw_data,  sample_rate):
    max_ind = len(raw_data) 
    start_ind = min(int(start * sample_rate), max_ind)
    end_ind = min(int(end * sample_rate), max_ind)
    return raw_data[start_ind: end_ind]


def Extract_Annotation_Data(file_name, root, diagnosis):
    tokens = file_name.split('_')
    recording_info = pd.DataFrame(data = [tokens], columns = ['Patient number', 'Recording index', 'Chest location','Acquisition mode','Recording equipment'])
    recording_annotations = pd.read_csv(os.path.join(root, file_name + '.txt'), names = ['Start', 'End', 'Crackles', 'Wheezes'], delimiter= '\t')
    diagnosis["Patient number"] = pd.to_numeric(diagnosis["Patient number"])
    recording_info["Patient number"] = pd.to_numeric(recording_info["Patient number"])
    recording_info =  recording_info.join(diagnosis.set_index('Patient number'), on = 'Patient number', how = 'left')

    return (recording_info, recording_annotations)

def get_sound_samples(recording_annotations, file_name, root, sample_rate):
    sample_data = [file_name]
    (rate, data) = read_wav_file(os.path.join(root, file_name + '.wav'), sample_rate)
    
    for i in range(len(recording_annotations.index)):
        row = recording_annotations.loc[i]
        start = row['Start']
        end = row['End']
        crackles = row['Crackles']
        wheezes = row['Wheezes']
        audio_chunk = slice_data(start, end, data, rate)
        sample_data.append((audio_chunk, start,end,crackles,wheezes))
    return sample_data

#Fits each respiratory cycle into a fixed length audio clip, splits may be performed and zero padding is added if necessary
#original:(arr,c,w) -> output:[(arr,c,w),(arr,c,w)]
def split_and_pad(original, desiredLength, sampleRate):
    output_buffer_length = int(desiredLength * sampleRate)
    soundclip = original[0]
    n_samples = len(soundclip)
    total_length = n_samples / sampleRate #length of cycle in seconds
    n_slices = int(np.ceil(total_length / desiredLength)) #get the minimum number of slices needed
    samples_per_slice = n_samples // n_slices
    src_start = 0 #Staring index of the samples to copy from the original buffer
    output = [] #Holds the resultant slices
    for i in range(n_slices):
        src_end = min(src_start + samples_per_slice, n_samples)
        length = src_end - src_start
        copy = generate_padded_samples(soundclip[src_start:src_end], output_buffer_length)
        output.append((copy, original[1]))
        src_start += length
    return output

def generate_padded_samples(source, output_length):
    copy = np.zeros(output_length, dtype = np.float32)
    src_length = len(source)
    frac = src_length / output_length
    if(frac < 0.5):
        #tile forward sounds to fill empty space
        cursor = 0
        while(cursor + src_length) < output_length:
            copy[cursor:(cursor + src_length)] = source[:]
            cursor += src_length
    else:
        copy[:src_length] = source[:]
    #
    return copy
def split_and_pad_and_apply_mel_spect(original, desiredLength, sampleRate, VTLP_alpha_range = None, VTLP_high_freq_range = None, n_repeats = 1):
    output = []
    for i in range(n_repeats):
        for d in original:
            lst_result = split_and_pad(d, desiredLength, sampleRate) #Time domain
            # if( (VTLP_alpha_range is None) | (VTLP_high_freq_range is None) ):
            #     #Do not apply VTLP
            #     VTLP_params = None
            # else:
            #     #Randomly generate VLTP parameters
            #     alpha = np.random.uniform(VTLP_alpha_range[0], VTLP_alpha_range[1])
            #     high_freq = np.random.uniform(VTLP_high_freq_range[0], VTLP_high_freq_range[1])
            #     VTLP_params = (alpha, high_freq)
            # freq_result = [sample2MelSpectrum(d, sampleRate, 50, VTLP_params) for d in lst_result] #Freq domain
            output.extend(lst_result)
    return output

def get_label(label):
    if label == (0,0):
        return 'Noise'
    if label == (1,1):
        return 'Both'
    if label == (0,1):
        return 'Wheez'
    return 'Cough'

def extract_all_training_samples(filenames, annotation_dict, root, target_rate, desired_length, recording_info_dict, train_test_ratio = 0.2):
    cycle_list = []
    label_list = []

    for file in filenames:
        data = get_sound_samples(annotation_dict[file], file, root, target_rate)
        
        for i in range(1,len(data)):
            cycle_list.append(data[i][0])
            label_list.append((recording_info_dict[file].Diagnosis.item(), get_label(data[i][3:])))
    assert len(cycle_list) == len(label_list)
    
    df_label = pd.DataFrame(label_list)

    #Sort into respective classes
    enc = OneHotEncoder(handle_unknown='ignore', sparse=False)
    df_label = enc.fit_transform(df_label)

    return cycle_list, df_label, enc
    # no_labels = [(cycle_list[i][0], label_list[i]) for i in range(len(label_list)) if ((cycle_list[i][1] == 0) & (cycle_list[i][2] == 0))]
    # c_only = [(cycle_list[i][0], label_list[i]) for i in range(len(label_list)) if ((cycle_list[i][1] == 1) & (cycle_list[i][2] == 0))]
    # w_only = [(cycle_list[i][0], label_list[i]) for i in range(len(label_list)) if ((cycle_list[i][1] == 0) & (cycle_list[i][2] == 1))]
    # c_w = [(cycle_list[i][0], label_list[i]) for i in range(len(label_list)) if ((cycle_list[i][1] == 1) & (cycle_list[i][2] == 1))]
    
    # return pd.DataFrame(data = no_labels[0,:], columns=['data', 'label']), pd.DataFrame(c_only, columns=['data', 'label']), pd.DataFrame(w_only, columns=['data', 'label']), pd.DataFrame(c_w, columns=['data', 'label']), enc