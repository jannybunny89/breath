#!/usr/bin/env python
# coding: utf-8

# In[1]:
#TODO
# - bigger model
# - learning rate
# - feature engineering
#  - check for meaningful downsampling
#  - fft, sma, normalization
# - voice modulation
# - check for differences regarding recording sources

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import data_handler
import wave
import math
import matplotlib.pyplot as plt
from collections import Counter
import scipy
from scipy import fftpack, signal
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit
try:
    import lzma
except ImportError:
    from backports import lzma, _lzma
import librosa

datadir = '/mnt/c/Users/janei/Downloads/archive'
root = f'{datadir}/respiratory_sound_database/Respiratory_Sound_Database/audio_and_txt_files/'

# read patient data and diagnosis and combine the information
df_no_diagnosis = pd.read_csv(f'{datadir}/demographic_info.txt', names = 
                 ['Patient number', 'Age', 'Sex' , 'Adult BMI (kg/m2)', 'Child Weight (kg)' , 'Child Height (cm)'],
                 delimiter = ' ')
diagnosis = pd.read_csv(f'{datadir}/respiratory_sound_database/Respiratory_Sound_Database/patient_diagnosis.csv', names = ['Patient number', 'Diagnosis'])
df =  df_no_diagnosis.join(diagnosis.set_index('Patient number'), on = 'Patient number', how = 'left')

# filter out recordings with less than 44.1kHz
filenames = [s.split('.')[0] for s in os.listdir(path = root) if '.txt' in s]
sample_rate_list = np.zeros(shape=(len(filenames),1))
for i in range(len(filenames)):
    wav = wave.open(f'{root}/{filenames[i]}.wav', mode = 'r')
    (sample_rate, data) = data_handler.extract2FloatArr(wav, f'{root}/{filenames[i]}.wav')
    sample_rate_list[i] = sample_rate
    
sample_rate_list = np.array(sample_rate_list)
deleteDex = np.where(sample_rate_list>=44100)


# In[8]:
filenames = [filenames[i] for i in deleteDex[0].tolist()]


# In[9]:


i_list = []
rec_annotations = []
rec_annotations_dict = {}
recording_info_dict = {}
for s in filenames:
    (i,a) = data_handler.Extract_Annotation_Data(s, root,diagnosis)
    i_list.append(i)
    rec_annotations.append(a)
    rec_annotations_dict[s] = a
    recording_info_dict[s] = i


# In[10]:


no_label_list = []
crack_list = []
wheeze_list = []
both_sym_list = []
filename_list = []
for f in filenames:
    d = rec_annotations_dict[f]
    no_labels = len(d[(d['Crackles'] == 0) & (d['Wheezes'] == 0)].index)
    n_crackles = len(d[(d['Crackles'] == 1) & (d['Wheezes'] == 0)].index)
    n_wheezes = len(d[(d['Crackles'] == 0) & (d['Wheezes'] == 1)].index)
    both_sym = len(d[(d['Crackles'] == 1) & (d['Wheezes'] == 1)].index)
    no_label_list.append(no_labels)
    crack_list.append(n_crackles)
    wheeze_list.append(n_wheezes)
    both_sym_list.append(both_sym)
    filename_list.append(f)

# In[13]:


duration_list = []
for i in range(len(rec_annotations)):
    current = rec_annotations[i]
    duration = current['End'] - current['Start']
    duration_list.extend(duration)

duration_list = np.array(duration_list)
plt.hist(duration_list, bins = 50)
print('longest cycle:{}'.format(max(duration_list)))
print('shortest cycle:{}'.format(min(duration_list)))
threshold = 5
print('Fraction of samples less than {} seconds:{}'.format(threshold,
    np.sum(duration_list < threshold)/len(duration_list)))

# In[19]:




    #Count of labels across all cycles, actual recording time also follows similar ratios
    #none:3642
    #crackles:1864 
    #wheezes:886
    #both:506
    # none_train, none_test = train_test_split(no_labels, test_size = train_test_ratio)
    # c_train, c_test  = train_test_split(c_only, test_size = train_test_ratio)
    # w_train, w_test  = train_test_split(w_only, test_size = train_test_ratio)
    # c_w_train, c_w_test  = train_test_split(c_w, test_size = train_test_ratio)
    
    #Training section (Data augmentation procedures)
    #Augment w_only and c_w groups to match the size of c_only
    #no_labels will be artifically reduced in the pipeline  later
    # w_stretch = w_train + augment_list(w_train, target_rate, 10 , 1) #
    # c_w_stretch = c_w_train + augment_list(c_w_train , target_rate, 10 , 1) 
    
    #Split up cycles into sound clips with fixed lengths so they can be fed into a CNN
    # vtlp_alpha = [0.9,1.1]
    # vtlp_upper_freq = [3200,3800]
    # print('Train_None')
    # train_none  = (data_handler.split_and_pad_and_apply_mel_spect(none_train, desired_length, target_rate) +
    #                data_handler.split_and_pad_and_apply_mel_spect(none_train, desired_length, target_rate, vtlp_alpha))
    # print('Train_C')
    # train_c = (data_handler.split_and_pad_and_apply_mel_spect(c_train, desired_length, target_rate) + 
    #            data_handler.split_and_pad_and_apply_mel_spect(c_train, desired_length, target_rate, vtlp_alpha, vtlp_upper_freq, n_repeats = 3) ) #original samples + VTLP
    # print('Train_W')
    # train_w = (data_handler.split_and_pad_and_apply_mel_spect(w_train, desired_length, target_rate) + 
    #            data_handler.split_and_pad_and_apply_mel_spect(w_train , desired_length, target_rate, vtlp_alpha , vtlp_upper_freq, n_repeats = 4)) #(original samples + time stretch) + VTLP
    # print('Train_CW')
    # train_c_w = (data_handler.split_and_pad_and_apply_mel_spect(c_w_train, desired_length, target_rate) + 
    #              data_handler.split_and_pad_and_apply_mel_spect(c_w_train, desired_length, target_rate, vtlp_alpha , vtlp_upper_freq, n_repeats = 7)) #(original samples + time stretch * 2) + VTLP
    
    # train_dict = {'none':train_none,'crackles':train_c,'wheezes':train_w, 'both':train_c_w}
    
    # #test section 
    # test_none  = split_and_pad_and_apply_mel_spect(none_test, desired_length, target_rate)
    # test_c = split_and_pad_and_apply_mel_spect(c_test, desired_length, target_rate)
    # #test_w = split_and_pad_and_apply_mel_spect(w_test, desired_length, target_rate)
    # test_w = split_and_pad_and_apply_mel_spect(w_test, desired_length, target_rate)
    # print('Train_CW')
    # test_c_w = split_and_pad_and_apply_mel_spect(c_w_test, desired_length, target_rate)
    
    # test_dict = {'none':test_none,'crackles':test_c,'wheezes':test_w, 'both':test_c_w}
    
    # return [train_dict, test_dict, enc]
#Same as above, but applies it to a list of samples
# def augment_list(audio_with_labels, sample_rate, percent_change, n_repeats):
#     augmented_samples = []
#     for i in range(n_repeats):
#         addition = [(gen_time_stretch(t[0], sample_rate, percent_change), t[1] ) for t in audio_with_labels]
#         augmented_samples.extend(addition)
#     return augmented_samples
# def gen_time_stretch(original, sample_rate, max_percent_change):
#     stretch_amount = 1 + np.random.uniform(-1,1) * (max_percent_change / 100)
#     (_, stretched) = resample(sample_rate, original, int(sample_rate * stretch_amount)) 
#     return stretched

# In[ ]:
# from scipy import signal

# from scipy.fft import fftshift
# (rate, data) = read_wav_file(os.path.join(root, filenames[0] + '.wav'), 44100)
# f, t, Sxx = signal.spectrogram(data, rate, nfft=512, nperseg=512)

# plt.pcolormesh(t, f, 10*np.log10(Sxx), shading='gouraud')

# plt.ylabel('Frequency [Hz]')

# plt.xlabel('Time [sec]')

# (rate, data) = read_wav_file(os.path.join(root, filenames[1] + '.wav'), 44100)
# f, t, Sxx = signal.spectrogram(data, rate, nfft=512, nperseg=512)

# plt.pcolormesh(t, f, 10*np.log10(Sxx), shading='gouraud')

# plt.ylabel('Frequency [Hz]')

# plt.xlabel('Time [sec]')

# (rate, data) = read_wav_file(os.path.join(root, filenames[2] + '.wav'), 44100)
# f, t, Sxx = signal.spectrogram(data, rate, nfft=512, nperseg=512)

# plt.pcolormesh(t, f, 10*np.log10(Sxx), shading='gouraud')

# plt.ylabel('Frequency [Hz]')

# plt.xlabel('Time [sec]')

# (rate, data) = read_wav_file(os.path.join(root, filenames[3] + '.wav'), 44100)
# f, t, Sxx = signal.spectrogram(data, rate, nfft=512, nperseg=512)

# plt.pcolormesh(t, f, 10*np.log10(Sxx), shading='gouraud')

# plt.ylabel('Frequency [Hz]')

# plt.xlabel('Time [sec]')

# plt.show()

# plt.plot(data)
# plt.show()

# # In[ ]
# from scipy.signal import butter, lfilter, freqz

# def butter_lowpass(cutoff, fs, order=5):
#     nyq = 0.5 * fs
#     normal_cutoff = cutoff / nyq
#     b, a = butter(order, normal_cutoff, btype='low', analog=False)
#     return b, a

# def butter_lowpass_filter(data, cutoff, fs, order=5):
#     b, a = butter_lowpass(cutoff, fs, order=order)
#     y = lfilter(b, a, data)
#     return y
# y = butter_lowpass_filter(data, 100, 44100)

# f, t, Sxx = signal.spectrogram(y, 100, nfft=512, nperseg=512)

# plt.pcolormesh(t, f, 10*np.log10(Sxx), shading='gouraud')

# plt.ylabel('Frequency [Hz]')

# plt.xlabel('Time [sec]')

# plt.show()

# In[ ]:
# here we need resampling

target_sample_rate = 44100 
sample_length_seconds = 0.5
sample_dict = data_handler.extract_all_training_samples(filenames, rec_annotations_dict, root, target_sample_rate, sample_length_seconds, recording_info_dict) #sample rate lowered to meet memory constraints
clips = sample_dict[0]
labels = sample_dict[1]
encoder = sample_dict[2]

# training clips contains all clips, test_clips are the labels. test_clips from 9 to 12 contain both, cough, noise, wheeze

# TODO: noise reduction
# bin_amount = 100000
# Fs = 44100 # sampling rate
# for diagnosis in range(0,8):
#     for action in range(8,12):
#         y_fft_total = np.zeros(bin_amount)
#         fr_total = np.linspace(start=0, num=bin_amount, stop=1)*np.linspace(start=0, num = bin_amount, stop=Fs/2)#Fs/2 * np.linspace(0,1,bin_amount)
#         # bins = np.arange(start=0, step=44.1/bin_amount, stop=44.1)*np.arange(start=0, stop=bin_amount)
#         # y_fft = np.histogram(fftpack.fft(training_clips[0]), bins=bins, range = (0,44100))
#         for i in np.where(np.logical_and(test_clips[:,action]==1,
#             test_clips[:,diagnosis]==1))[0]:
#             L = len(training_clips[i])
#             t = np.arange(0,len(training_clips[i])/Fs,1/Fs)
#             n = np.size(t)
#             fr = Fs/2 * np.linspace(0,1,int(n/2))
#             y_fft = fftpack.fft(training_clips[i])
#             y_m = 2/n * abs(y_fft[0:np.size(fr)])
#             # bins = np.linspace(start=0, num=bin_amount, stop=1)*np.linspace(start=0, num = bin_amount, stop=np.size(fr))
#             # occurence = np.histogram(y_m, bins=bins, range=(0,np.size(fr)))
#             # freqs /= L/Fs
#             for j,x in enumerate(y_m):
#                 bin = np.where(fr[j]<=fr_total)[0][0]
#                 y_fft_total[bin] = x

#         focus_on = int(bin_amount/4)
#         start_at = 0
#         plt.clf()
#         plt.plot(fr_total[start_at:start_at+focus_on-1],
#             10*np.log10(y_fft_total[start_at:start_at+focus_on-1]/np.sum(test_clips[:,action]==1)+1))
#         plt.show()
#         plt.savefig(f'dist_{enc.categories_[0][diagnosis]}_{enc.categories_[1][action-8]}.png')
#         np.savetxt(f'dist_{enc.categories_[0][diagnosis]}_{enc.categories_[1][action-8]}_occ.dat', y_fft_total)
#         np.savetxt(f'dist_{enc.categories_[0][diagnosis]}_{enc.categories_[1][action-8]}_freq.dat', fr_total)

#we can savely downsample to 4410Hz
fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(15,5))
ax[0].plot(scipy.signal.decimate(clips[0], 10, n=None, ftype='iir', axis=- 1, zero_phase=True))
ax[1].plot(clips[0])

clips = [scipy.signal.decimate(x, 10, n=None, ftype='iir', axis=- 1, zero_phase=True) for x in clips]
reduced_sample_rate = 4100
# create windows from the data
window_length = 4100 # [Hz]
overlap = int(window_length/5)

windows = []
windows_labels = []
for j,i in enumerate(clips):
    ind = 0
    while ind + window_length-overlap < len(i):
        windows.append(i[ind:ind+window_length])
        windows_labels.append(labels[j])
        ind += window_length-overlap
windows = np.array(windows)
windows_labels = np.array(windows_labels)

# feature computation. TODO: make shape correct
for x in windows:
    spectral_centroids = librosa.feature.spectral_centroid(x, sr=reduced_sample_rate)[0]
    spectral_rolloff = librosa.feature.spectral_rolloff(x+0.01, sr=reduced_sample_rate)[0]
    spectral_bandwidth_2 = librosa.feature.spectral_bandwidth(x+0.01, sr=reduced_sample_rate)[0]
    spectral_bandwidth_3 = librosa.feature.spectral_bandwidth(x+0.01, sr=reduced_sample_rate, p=3)[0]
    spectral_bandwidth_4 = librosa.feature.spectral_bandwidth(x+0.01, sr=reduced_sample_rate, p=4)[0]
    zero_crossings = librosa.zero_crossings(x, pad=False)
    mfccs = librosa.feature.mfcc(x, sr=reduced_sample_rate)
    chromagram = librosa.feature.chroma_stft(x, sr=reduced_sample_rate)

# split the data into train, test and validation. Assert distribution conservation
sss = StratifiedShuffleSplit(n_splits=5, test_size=0.25, random_state=0)
for train_index, test_index in sss.split(windows, windows_labels[:,:8]):
    X_train, X_test = windows[train_index], windows[test_index]
    y_train, y_test = windows_labels[train_index], windows_labels[test_index]

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(15,5))
ax[0].hist(np.argmax(y_train[:,:8], axis=1))
ax[1].hist(np.argmax(y_test[:,:8], axis=1))
plt.savefig('splitdatahist.png')


# todo: implement feature computation
#spectral centroid

# In[ ]:



#Example of tiled sound samples
sample_height = training_clips['none'][0][0].shape[0]



# In[ ]:




import scipy.signal
import keras

#Used for validation set
class data_generator(keras.utils.Sequence):
    #sound_clips = [[none],[crackles],[wheezes],[both]]
    #strides: How far the sampling index for each category is advanced for each step
    def __init__(self, sound_clips, strides, batch_size, roll = True):
        merged = []
        for arr in sound_clips:
            merged.extend(arr)
        np.random.shuffle(merged)
        self.clips = merged
        self.nclips = len(merged)
        self.roll = roll
        self.batch_size = batch_size
        labels = [np.argmax(x[1]) for x in self.clips]
        self.indices = []

        for j in np.unique(labels):
            self.indices.append([labels.index(j)])
        
        for i in range(0,len(self.indices)): 
            for x in range(0,len(self.clips)):
                if labels[x]==np.unique(labels)[i]:
                    self.indices[i].append(x)
    
    def __len__(self):
        return int(np.floor(len(self.clips)))
    
    def  __getitem__(self, index):
        i = 0
        X,y = [],[]
        for b in range(self.batch_size):
            clip = self.clips[self.indices[i][(index+b)%len(self.indices[i])]]
            i = (i + 1) % len(self.indices)
            if(self.roll):
                #s = (self.rollFFT(clip))
                X.append(clip[0])#s[0])
                y.append(clip[1])
            else:
                X.append(clip[0])
                y.append(clip[1])
                
        return np.reshape(X, (self.batch_size,sample_height, 1)),np.reshape(y, (self.batch_size,-1))

# In[ ]:
from collections import Counter
from numpy import argmax
batch_size = 128
n_epochs = 15
[none_train, c_train, w_train, c_w_train] = [training_clips['none'], training_clips['crackles'], training_clips['wheezes'], training_clips['both']]
[none_test, c_test, w_test,c_w_test] =  [test_clips['none'], test_clips['crackles'], test_clips['wheezes'], test_clips['both']]

np.random.shuffle(none_train)
np.random.shuffle(c_train)
np.random.shuffle(w_train)
np.random.shuffle(c_w_train)

#Data pipeline objects
train_gen = data_generator([none_train, c_train, w_train, c_w_train], [1,1,1,1], batch_size)
test_gen = data_generator([none_test, c_test, w_test,c_w_test], [1,1,1,1], batch_size)
letter_counts_none = np.sum([a_tuple[1] for a_tuple in training_clips['none']],0)#Counter([x[1] for x in training_clips['none']])
letter_counts_crackles = np.sum([a_tuple[1] for a_tuple in training_clips['crackles']],0)#Counter([enc.inverse_transform(argmax(x[1])) for x in training_clips['crackles']])
letter_counts_wheezes = np.sum([a_tuple[1] for a_tuple in training_clips['wheezes']],0)#Counter([x[1] for x in training_clips['wheezes']])
letter_counts_both = np.sum([a_tuple[1] for a_tuple in training_clips['both']],0)#Counter([x[1] for x in training_clips['both']])

sum_all = letter_counts_none+letter_counts_crackles+letter_counts_wheezes+letter_counts_both
weights = 1 / (sum_all/np.sum(sum_all))
weights[np.where(weights==np.inf)] = np.max(weights[weights!=np.inf])
class_weights = dict(enumerate(weights.flatten(), 0))

from sklearn.utils import class_weight
class_weights = class_weight.compute_class_weight(class_weight=class_weights, classes=np.arange(0,8), y=np.arange(0,8))
class_weights = dict(enumerate(class_weights))


# In[ ]:


from keras.models import Sequential
from keras import optimizers
from keras import backend as K
from keras.layers import LSTM, Conv2D,Conv1D, Dense, Activation, Dropout, MaxPool2D, Flatten, LeakyReLU
import tensorflow as tf
K.clear_session()

model = Sequential()
model.add(LSTM(12, input_shape=(sample_height, 1)))
model.add(Dense(8, activation = 'softmax'))
model.compile(optimizer =  'adam' , loss = 'categorical_crossentropy', metrics = ['acc'])


# In[ ]:


stats = model.fit_generator(generator=train_gen, steps_per_epoch = len(train_gen) // batch_size,
    validation_data = test_gen, class_weight=class_weights, validation_steps = len(test_gen) // batch_size, 
    epochs = n_epochs, use_multiprocessing=False,
    workers=2)
model.save('mymodel.h5')



# In[ ]:

model = keras.models.load_model("mymodel.h5")

# test_set = test_gen.generate_keras(test_gen.n_available_samples()).__next__()
# predictions = model.predict_generator(test_gen)
# predictions = np.argmax(predictions, axis = 1)
labels = [(np.argmax(model.predict(np.reshape(x[0],(1,sample_height,1))),axis=1),np.argmax(x[1])) for x in test_gen.clips]
out = [x[0] for x in labels]
truth = [x[1] for x in labels]

# In[ ]:


from sklearn.metrics import classification_report, confusion_matrix

plt.figure()
plt.plot(out)
plt.plot(truth)
plt.show()
plt.savefig('results.png')

print(classification_report(truth, out, target_names = enc.categories_))
print(confusion_matrix(truth, out))


# %%
